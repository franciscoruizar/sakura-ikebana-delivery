package ar.edu.ungs.game.player;

import ar.edu.ungs.game.environment.Environment;
import ar.edu.ungs.game.power.PowerDrawer;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public final class PlayerDrawer {
    private static final String      PLAYER_FILE = "src/main/resources/player.png";
    private static final String      FLOWER_FILE = "src/main/resources/flower.png";
    private final        Environment environment;
    private final        PowerDrawer powerDrawer;

    public PlayerDrawer(Environment environment) {
        this.environment = environment;
        this.powerDrawer = new PowerDrawer(environment);
    }

    public void draw(Player player) throws IOException {
        if (player.isHasFlower()) {
            this.drawPlayerWithFlower(environment, player);
        } else {
            this.drawPlayer(environment, player);
        }
    }

    public void drawPlayer(Environment environment, Player player) throws IOException {
        Image image = ImageIO.read(new File(PLAYER_FILE));
        environment.drawImage(image, player.x(), player.y(), 0);

        if (player.hasPower()) {
            powerDrawer.draw(player.power());
        }
    }

    public void drawPlayerWithFlower(Environment environment, Player player) throws IOException {
        Image image = ImageIO.read(new File(FLOWER_FILE));
        environment.drawImage(image, player.x() - 2, player.y(), 180);

        drawPlayer(environment, player);
    }
}
