package ar.edu.ungs.game.player;

import ar.edu.ungs.game.power.Power;

public final class Player {
    private static final double  STEPS        = 1;
    private static final int     HEIGHT       = 20;
    private static final int     WIDTH        = 22;
    private static final int     NUMBER_LIVES = 3;
    private final        double  xInitial;
    private final        double  yInitial;
    private              double  x;
    private              double  y;
    private              boolean hasFlower;
    private              Power   power;
    private              int     lives;

    public Player(double x, double y) {
        this.x         = x;
        this.y         = y;
        this.xInitial  = x;
        this.yInitial  = y;
        this.hasFlower = false;
        this.power     = null;
        this.lives     = NUMBER_LIVES;
    }

    public static double STEPS() {
        return STEPS;
    }

    public static int HEIGHT() {
        return HEIGHT;
    }

    public static int WIDTH() {
        return WIDTH;
    }

    public double x() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void accumulateX(double value) {
        this.x += value;
    }

    public void deAccumulateX(double value) {
        this.x -= value;
    }

    public double y() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void accumulateY(double value) {
        this.y += value;
    }

    public void deAccumulateY(double value) {
        this.y -= value;
    }

    public boolean isHasFlower() {
        return hasFlower;
    }

    public void setHasFlower(boolean hasFlower) {
        this.hasFlower = hasFlower;
    }

    public boolean hasPower() {
        return this.power != null;
    }

    public void createPower() {
        this.power = new Power(x, y);
    }

    public void deletePower() {
        this.power = null;
    }

    public Power power() {
        return power;
    }

    public int lives() {
        return lives;
    }

    public void kill() {
        this.lives -= 1;
        this.x = this.xInitial;
        this.y = this.yInitial;
        this.deletePower();
        this.hasFlower = false;
    }
}
