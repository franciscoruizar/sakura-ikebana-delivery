package ar.edu.ungs.game.player;

import ar.edu.ungs.game.environment.Environment;
import ar.edu.ungs.game.environment.Key;
import ar.edu.ungs.game.power.PowerMotionTrigger;

import java.io.IOException;

public final class PlayerMotionTrigger {
    private final Environment        environment;
    private final PowerMotionTrigger powerMotionTrigger;
    private       Key                lastKey;

    public PlayerMotionTrigger(Environment environment) {
        this.environment        = environment;
        this.powerMotionTrigger = new PowerMotionTrigger(environment);
        this.lastKey            = Key.EMPTY;
    }

    public void trig(Player player, boolean isColliding) throws IOException {
        if (isColliding) {
            actionsColliding(player);
        } else {
            actions(player);
        }
    }

    private void actionsColliding(Player player) {
        if (environment.isPressed(Key.UP)) {
            down(player);
        } else if (environment.isPressed(Key.DOWN)) {
            up(player);
        } else if (environment.isPressed(Key.RIGHT)) {
            left(player);
        } else if (environment.isPressed(Key.LEFT)) {
            right(player);
        }
    }

    private void actions(Player player) {
        if (environment.isPressed(Key.UP)) {
            up(player);
            lastKey = Key.UP;
        } else if (environment.isPressed(Key.DOWN)) {
            down(player);
            lastKey = Key.DOWN;
        } else if (environment.isPressed(Key.RIGHT)) {
            right(player);
            lastKey = Key.RIGHT;
        } else if (environment.isPressed(Key.LEFT)) {
            left(player);
            lastKey = Key.LEFT;
        }

        if (environment.isPressed(Key.SPACE)) {
            power(player);
        }

        if (player.hasPower()) {
            powerMotionTrigger.trig(player);
        }
    }

    private void power(Player player) {
        if (!player.hasPower() && !lastKey.equals(Key.EMPTY)) {
            player.createPower();
            powerMotionTrigger.setLastKey(lastKey);
        }
    }

    private void right(Player player) {
        if (player.x() < environment.width()) {
            player.accumulateX(Player.STEPS());
        }
    }

    private void left(Player player) {
        if (player.x() > 0) {
            player.deAccumulateX(Player.STEPS());
        }
    }

    private void up(Player player) {
        if (player.y() > 10) {
            player.deAccumulateY(Player.STEPS());
        }
    }

    private void down(Player player) {
        if (player.y() < environment.height()) {
            player.accumulateY(Player.STEPS());
        }
    }
}
