package ar.edu.ungs.game.scores;

import ar.edu.ungs.game.player.Player;

public final class ScoreBoardLives {
    private final Integer x;
    private final Integer y;
    private final Player  player;

    public ScoreBoardLives(Integer x, Integer y, Player player) {
        this.x      = x;
        this.y      = y;
        this.player = player;
    }

    public Integer lives() {
        return player.lives();
    }

    public Integer x() {
        return x;
    }

    public Integer y() {
        return y;
    }
}
