package ar.edu.ungs.game.scores;

public final class ScoreBoardEnemiesKilled {
    private final Integer x;
    private final Integer y;
    private       Integer enemiesKilled;

    public ScoreBoardEnemiesKilled(Integer x, Integer y) {
        this.x             = x;
        this.y             = y;
        this.enemiesKilled = 0;
    }

    public Integer enemiesKilled() {
        return enemiesKilled;
    }

    public void kill() {
        this.enemiesKilled += 1;
    }

    public Integer x() {
        return x;
    }

    public Integer y() {
        return y;
    }
}
