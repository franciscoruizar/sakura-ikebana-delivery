package ar.edu.ungs.game.scores;

public final class ScoreBoardFlowersDelivered {
    private static final Integer VALUE_POINTS = 5;
    private final        Integer x;
    private final        Integer y;
    private              Integer points;

    public ScoreBoardFlowersDelivered(Integer x, Integer y) {
        this.x      = x;
        this.y      = y;
        this.points = 0;
    }

    public Integer points() {
        return points;
    }

    public void add() {
        this.points += VALUE_POINTS;
    }

    public void add(int value) {
        this.points += value;
    }

    public Integer x() {
        return x;
    }

    public Integer y() {
        return y;
    }
}
