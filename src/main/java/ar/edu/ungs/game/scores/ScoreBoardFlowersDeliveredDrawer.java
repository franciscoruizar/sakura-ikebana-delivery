package ar.edu.ungs.game.scores;

import ar.edu.ungs.game.environment.Environment;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public final class ScoreBoardFlowersDeliveredDrawer {
    private static final String FLOWER_FILE = "src/main/resources/flower.png";

    private final Environment environment;

    public ScoreBoardFlowersDeliveredDrawer(Environment environment) {
        this.environment = environment;
    }

    public void draw(ScoreBoardFlowersDelivered scoreBoard) throws IOException {
        environment.drawImage(ImageIO.read(new File(FLOWER_FILE)), scoreBoard.x() - 10, scoreBoard.y() + 20, 0, 2);
        environment.writeText(scoreBoard.points().toString(), scoreBoard.x() - 45, scoreBoard.y() + 25);
    }
}
