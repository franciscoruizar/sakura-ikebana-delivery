package ar.edu.ungs.game.scores;

import ar.edu.ungs.game.environment.Environment;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public final class ScoreBoardEnemiesKilledDrawer {
    private static final String ENEMY_FILE = "src/main/resources/ninja.png";

    private final Environment environment;

    public ScoreBoardEnemiesKilledDrawer(Environment environment) {
        this.environment = environment;
    }

    public void draw(ScoreBoardEnemiesKilled scoreBoard) throws IOException {
        environment.drawImage(ImageIO.read(new File(ENEMY_FILE)), scoreBoard.x() + 15, scoreBoard.y() + 20, 0, 2);
        environment.writeText(scoreBoard.enemiesKilled().toString(), scoreBoard.x() + 40, scoreBoard.y() + 25);
    }
}
