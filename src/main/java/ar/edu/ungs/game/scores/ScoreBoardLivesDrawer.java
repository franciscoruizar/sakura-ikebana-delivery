package ar.edu.ungs.game.scores;

import ar.edu.ungs.game.environment.Environment;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public final class ScoreBoardLivesDrawer {
    private static final String HEART_FILE = "src/main/resources/heart.png";

    private final Environment environment;

    public ScoreBoardLivesDrawer(Environment environment) {
        this.environment = environment;
    }

    public void draw(ScoreBoardLives scoreBoard) throws IOException {
        for (int i = 1; i <= scoreBoard.lives(); i++) {
            environment.drawImage(ImageIO.read(new File(HEART_FILE)), scoreBoard.x() - 10, scoreBoard.y() + (i * 30), 0, 0.1);
        }
    }
}
