package ar.edu.ungs.game.houses;

import ar.edu.ungs.game.environment.Environment;

import java.awt.*;

public final class HouseDrawer {
    private final Environment environment;

    public HouseDrawer(Environment environment) {
        this.environment = environment;
    }

    public void draw(House house) {
        environment.drawRectangle(house.x(), house.y(), House.WIDTH(), House.HEIGHT(), 0, house.isRandomPicked() ? Color.RED : Color.PINK);
    }
}
