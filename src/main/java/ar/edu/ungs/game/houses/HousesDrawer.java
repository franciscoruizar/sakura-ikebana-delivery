package ar.edu.ungs.game.houses;

import ar.edu.ungs.game.blocks.Block;
import ar.edu.ungs.game.blocks.Blocks;
import ar.edu.ungs.game.environment.Environment;

import java.io.IOException;

public final class HousesDrawer {
    private final HouseDrawer drawer;

    public HousesDrawer(Environment environment) {
        drawer = new HouseDrawer(environment);
    }

    public void draw(Blocks blocks) throws IOException {
        for (Block item : blocks.values())
            for (House house : item.houses())
                drawer.draw(house);
    }
}
