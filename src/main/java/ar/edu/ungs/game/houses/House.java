package ar.edu.ungs.game.houses;

public final class House {
    private final static int     HEIGHT = 30;
    private final static int     WIDTH  = 30;
    private final        double  x;
    private final        double  y;
    private final        boolean randomPicked;

    public House(double x, double y, boolean randomPicked) {
        this.x            = x;
        this.y            = y;
        this.randomPicked = randomPicked;
    }


    public static int HEIGHT() {
        return HEIGHT;
    }

    public static int WIDTH() {
        return WIDTH;
    }

    public double x() {
        return x;
    }

    public double y() {
        return y;
    }

    public boolean isRandomPicked() {
        return randomPicked;
    }
}
