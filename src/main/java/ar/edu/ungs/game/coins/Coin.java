package ar.edu.ungs.game.coins;

public final class Coin {
    private final static int    VALUE  = 5;
    private static final int    HEIGHT = 20;
    private static final int    WIDTH  = 22;
    private              double y;
    private              double x;

    public Coin(int x) {
        this.y = 10;
        this.x = x;
    }

    public static int VALUE() {
        return VALUE;
    }

    public static int HEIGHT() {
        return HEIGHT;
    }

    public static int WIDTH() {
        return WIDTH;
    }

    public double y() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double x() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }
}
