package ar.edu.ungs.game.coins;

import java.util.Random;

public final class CoinPositionCalculator {
    public void calculate(Coin coin, int rows, double spaceX, double spaceY) {
        int random = new Random().nextInt(rows);
        coin.setX(random * spaceX + 5);
        coin.setY(random * spaceY + 4);
    }
}
