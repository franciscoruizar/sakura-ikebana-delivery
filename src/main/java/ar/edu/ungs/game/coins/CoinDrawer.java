package ar.edu.ungs.game.coins;

import ar.edu.ungs.game.environment.Environment;

import java.awt.*;
import java.io.IOException;

public final class CoinDrawer {
    private final Environment environment;

    public CoinDrawer(Environment environment) {
        this.environment = environment;
    }

    public void draw(Coin coin) throws IOException {
        environment.drawCircle(coin.x(), coin.y(), 10, Color.ORANGE);
    }
}
