package ar.edu.ungs.game.collisions;

import ar.edu.ungs.game.enemies.Enemy;
import ar.edu.ungs.game.power.Power;

public final class PowerEnemyColliderCalculator {
    public boolean calculate(Power power, Enemy enemy) {
        return enemy.x() + Enemy.WIDTH() / 2 + 13 > power.x() &&
            enemy.y() + Enemy.HEIGHT() / 2 + 10 > power.y() &&
            power.x() + Power.WIDTH() * 3 > enemy.x() &&
            power.y() + Power.HEIGHT() * 3 > enemy.y();
    }
}
