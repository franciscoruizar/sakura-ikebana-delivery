package ar.edu.ungs.game.collisions;

import ar.edu.ungs.game.houses.House;
import ar.edu.ungs.game.player.Player;

public final class PlayerHouseColliderCalculator {
    public boolean calculate(Player player, House house) {
        return house.x() + House.WIDTH() > player.x() &&
            house.y() + House.HEIGHT() > player.y() &&
            player.x() + Player.WIDTH() > house.x() &&
            player.y() + Player.HEIGHT() > house.y();
    }
}
