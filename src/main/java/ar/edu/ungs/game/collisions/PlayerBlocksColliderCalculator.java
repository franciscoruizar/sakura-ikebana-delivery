package ar.edu.ungs.game.collisions;

import ar.edu.ungs.game.blocks.Block;
import ar.edu.ungs.game.blocks.Blocks;
import ar.edu.ungs.game.houses.House;
import ar.edu.ungs.game.player.Player;
import ar.edu.ungs.game.player.PlayerMotionTrigger;
import ar.edu.ungs.game.scores.ScoreBoardFlowersDelivered;

import java.io.IOException;

public final class PlayerBlocksColliderCalculator {
    private final PlayerBlockColliderCalculator calculator;
    private final PlayerHouseColliderCalculator houseCalculator;

    public PlayerBlocksColliderCalculator() {
        this.calculator      = new PlayerBlockColliderCalculator();
        this.houseCalculator = new PlayerHouseColliderCalculator();
    }

    public void calculate(
        Player player,
        Blocks blocks,
        ScoreBoardFlowersDelivered scoreBoard,
        PlayerMotionTrigger playerMotionTrigger
    ) throws IOException {
        for (int i = blocks.values().size() - 1; i >= 0; i--) {
            boolean isColliding = calculator.calculate(player, blocks.values().get(i));
            if (isColliding) {
                Block block = blocks.values().get(i);
                if (player.isHasFlower() && block.isRandomPicked()) {
                    for (House house : block.houses()) {
                        if (houseCalculator.calculate(player, house)) {
                            if (house.isRandomPicked()) {

                                if (block.houses().indexOf(house) == 0) {
                                    player.setX(house.x() - 38);
                                } else if (block.houses().indexOf(house) == 1) {
                                    player.setX(house.x() + 40);
                                } else if (block.houses().indexOf(house) == 2){
                                    player.setY(house.y() + 38);
                                }

                                player.setHasFlower(false);
                                scoreBoard.add();
                                blocks.generateValues();
                            }

                            playerMotionTrigger.trig(player, true);
                        }
                    }
                } else {
                    playerMotionTrigger.trig(player, true);
                }
            }
        }
    }
}
