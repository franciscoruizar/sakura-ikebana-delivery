package ar.edu.ungs.game.collisions;

import ar.edu.ungs.game.blocks.Block;
import ar.edu.ungs.game.blocks.Blocks;
import ar.edu.ungs.game.player.Player;

public final class PowerBlocksColliderCalculator {
    private final PowerBlockColliderCalculator calculator;

    public PowerBlocksColliderCalculator() {
        this.calculator = new PowerBlockColliderCalculator();
    }

    public void calculate(Blocks blocks, Player player) {
        if (!player.hasPower()) {
            return;
        }

        for (Block block : blocks.values()) {
            boolean isColliding = calculator.calculate(player.power(), block);

            if (isColliding) {
                player.deletePower();
                return;
            }
        }
    }
}
