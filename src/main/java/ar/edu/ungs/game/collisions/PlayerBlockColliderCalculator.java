package ar.edu.ungs.game.collisions;

import ar.edu.ungs.game.blocks.Block;
import ar.edu.ungs.game.player.Player;

public final class PlayerBlockColliderCalculator {
    public boolean calculate(Player player, Block block) {
        return block.x() + Block.WIDTH() / 2 + 13 > player.x() &&
            block.y() + Block.HEIGHT() / 2 + 10 > player.y() &&
            player.x() + Player.WIDTH() * 3 > block.x() &&
            player.y() + Player.HEIGHT() * 3 > block.y();
    }
}
