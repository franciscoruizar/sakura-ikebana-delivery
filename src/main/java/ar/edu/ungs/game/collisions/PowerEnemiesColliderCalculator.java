package ar.edu.ungs.game.collisions;

import ar.edu.ungs.game.enemies.Enemies;
import ar.edu.ungs.game.player.Player;
import ar.edu.ungs.game.scores.ScoreBoardEnemiesKilled;

public final class PowerEnemiesColliderCalculator {
    private final PowerEnemyColliderCalculator calculator;

    public PowerEnemiesColliderCalculator() {
        this.calculator = new PowerEnemyColliderCalculator();
    }

    public void calculate(Enemies enemies, Player player, ScoreBoardEnemiesKilled scoreBoard) {
        for (int i = enemies.values().length - 1; i >= 0; i--) {
            if (
                player.hasPower() &&
                    enemies.values()[i] != null &&
                    calculator.calculate(player.power(), enemies.values()[i])
            ) {
                enemies.kill(i);
                player.deletePower();
                scoreBoard.kill();
            }
        }
    }
}
