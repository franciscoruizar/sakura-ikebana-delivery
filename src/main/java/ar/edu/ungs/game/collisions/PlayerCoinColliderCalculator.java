package ar.edu.ungs.game.collisions;

import ar.edu.ungs.game.coins.Coin;
import ar.edu.ungs.game.coins.CoinPositionCalculator;
import ar.edu.ungs.game.player.Player;
import ar.edu.ungs.game.scores.ScoreBoardFlowersDelivered;

public final class PlayerCoinColliderCalculator {
    private final CoinPositionCalculator positionCalculator;

    public PlayerCoinColliderCalculator() {
        this.positionCalculator = new CoinPositionCalculator();
    }

    public void calculate(Player player,
                          Coin coin,
                          ScoreBoardFlowersDelivered scoreBoardFlowersDelivered,
                          int rows,
                          double spaceX,
                          double spaceY) {

        if (
            coin.x() + Coin.WIDTH() / 2 > player.x() &&
                coin.y() + Coin.HEIGHT() / 2 > player.y() &&
                player.x() + Player.WIDTH() / 2 > coin.x() &&
                player.y() + Player.HEIGHT() / 2 > coin.y()
        ) {

            positionCalculator.calculate(coin, rows, spaceX, spaceY);
            scoreBoardFlowersDelivered.add(Coin.VALUE());
        }
    }
}
