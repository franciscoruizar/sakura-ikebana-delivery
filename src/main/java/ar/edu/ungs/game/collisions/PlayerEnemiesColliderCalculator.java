package ar.edu.ungs.game.collisions;

import ar.edu.ungs.game.enemies.Enemies;
import ar.edu.ungs.game.environment.Environment;
import ar.edu.ungs.game.player.Player;

public final class PlayerEnemiesColliderCalculator {
    private final Environment                   environment;
    private final PlayerEnemyColliderCalculator calculator;

    public PlayerEnemiesColliderCalculator(Environment environment) {
        this.environment = environment;
        this.calculator  = new PlayerEnemyColliderCalculator();
    }

    public void calculate(Player player, Enemies enemies) {
        for (int i = 0; i < enemies.values().length; i++) {
            if (enemies.values()[i] != null && calculator.calculate(player, enemies.values()[i])) {
                if (player.lives() == 0) {
                    environment.dispose();
                } else {
                    player.kill();
                }
            }
        }
    }
}
