package ar.edu.ungs.game.collisions;

import ar.edu.ungs.game.blocks.Block;
import ar.edu.ungs.game.power.Power;

public final class PowerBlockColliderCalculator {
    public boolean calculate(Power power, Block block) {
        return block.x() + Block.WIDTH() / 2 + 13 > power.x() &&
            block.y() + Block.HEIGHT() / 2 + 10 > power.y() &&
            power.x() + Power.WIDTH() * 3 > block.x() &&
            power.y() + Power.HEIGHT() * 3 > block.y();
    }
}
