package ar.edu.ungs.game.collisions;

import ar.edu.ungs.game.flower_shop.FlowerShop;
import ar.edu.ungs.game.player.Player;
import ar.edu.ungs.game.player.PlayerMotionTrigger;

import java.io.IOException;

public final class PlayerFlowerShopColliderCalculator {
    public boolean calculate(Player player,
                             FlowerShop flowerShop,
                             PlayerMotionTrigger playerMotionTrigger) throws IOException {
        boolean response = flowerShop.x() + FlowerShop.WIDTH() / 2 > player.x() &&
            flowerShop.y() + FlowerShop.HEIGHT() / 2 > player.y() &&
            player.x() + Player.WIDTH() / 2 > flowerShop.x() &&
            player.y() + Player.HEIGHT() / 2 > flowerShop.y();

        if (response) {
            player.setHasFlower(true);
            playerMotionTrigger.trig(player, true);
        }

        return response;
    }
}
