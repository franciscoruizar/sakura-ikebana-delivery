package ar.edu.ungs.game.collisions;

import ar.edu.ungs.game.enemies.Enemy;
import ar.edu.ungs.game.player.Player;

public final class PlayerEnemyColliderCalculator {
    public boolean calculate(Player player, Enemy enemy) {
        return enemy.x() + Enemy.WIDTH() / 2 > player.x() &&
            enemy.y() + Enemy.HEIGHT() / 2 > player.y() &&
            player.x() + Player.WIDTH() / 2 > enemy.x() &&
            player.y() + Player.HEIGHT() / 2 > enemy.y();
    }
}
