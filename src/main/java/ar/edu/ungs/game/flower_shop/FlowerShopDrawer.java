package ar.edu.ungs.game.flower_shop;

import ar.edu.ungs.game.environment.Environment;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public final class FlowerShopDrawer {
    private final static String      FLOWER_SHOP_FILE = "src/main/resources/flower-shop.png";
    private final        Environment environment;

    public FlowerShopDrawer(Environment environment) {
        this.environment = environment;
    }

    public void draw(FlowerShop shop) throws IOException {
        Image image = ImageIO.read(new File(FLOWER_SHOP_FILE));
        environment.drawImageWithCenter(image, shop.x(), shop.y(), shop.x() / 2, shop.y() / 2, 0, 0.05);
    }
}
