package ar.edu.ungs.game.flower_shop;

public final class FlowerShop {
    private final static int    HEIGHT = 22;
    private final static int    WIDTH  = 22;
    private final        double x;
    private final        double y;

    public FlowerShop(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static int HEIGHT() {
        return HEIGHT;
    }

    public static int WIDTH() {
        return WIDTH;
    }

    public double x() {
        return x;
    }

    public double y() {
        return y;
    }
}
