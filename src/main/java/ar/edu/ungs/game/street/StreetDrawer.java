package ar.edu.ungs.game.street;

import ar.edu.ungs.game.environment.Environment;

import java.awt.*;

public final class StreetDrawer {
    private final Environment environment;

    public StreetDrawer(Environment environment) {
        this.environment = environment;
    }

    public void draw() {
        environment.drawRectangle(
            environment.width() / 2,
            environment.height() / 2,
            environment.width() + 100,
            environment.height() + 300,
            0,
            Color.gray
        );
    }
}
