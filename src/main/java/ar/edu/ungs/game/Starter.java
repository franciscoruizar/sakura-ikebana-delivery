package ar.edu.ungs.game;

import ar.edu.ungs.game.environment.Game;

public final class Starter {
    public static void main(String[] args) {
        Game.start();
    }
}
