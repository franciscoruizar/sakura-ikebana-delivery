package ar.edu.ungs.game.blocks;


import ar.edu.ungs.game.environment.Tools;
import ar.edu.ungs.game.houses.House;

import java.util.ArrayList;
import java.util.List;

public final class Block {
    private final static int         HEIGHT = 90;
    private final static int         WIDTH  = 115;
    private final        double      x;
    private final        double      y;
    private final        boolean     randomPicked;
    private final        List<House> houses;

    public Block(double x, double y, boolean randomPicked) {
        this.x            = x;
        this.y            = y;
        this.randomPicked = randomPicked;
        houses            = new ArrayList<>();

        createHouses();
    }

    public static int HEIGHT() {
        return HEIGHT;
    }

    public static int WIDTH() {
        return WIDTH;
    }

    private void createHouses() {
        int randomHouse = 0;

        if (randomPicked) {
            randomHouse = Tools.random(1, 3);
        }

        houses.add(new House(this.x - 30, this.y - 20, randomHouse == 1));
        houses.add(new House(this.x + 30, this.y - 20, randomHouse == 2));
        houses.add(new House(this.x, this.y + 20, randomHouse == 3));
    }

    public double x() {
        return x;
    }

    public double y() {
        return y;
    }

    public boolean isRandomPicked() {
        return randomPicked;
    }

    public List<House> houses() {
        return houses;
    }
}
