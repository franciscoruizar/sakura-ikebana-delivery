package ar.edu.ungs.game.blocks;

import ar.edu.ungs.game.environment.Tools;

import java.util.ArrayList;
import java.util.List;

public final class Blocks {
    private final Integer     columns;
    private final Integer     rows;
    private       List<Block> values;

    public Blocks(Integer rows, Integer columns) {
        this.columns = columns;
        this.rows    = rows;
        generateValues();
    }

    public void generateValues() {
        this.values = new ArrayList<>();
        final int randomRow    = Tools.random(0, rows - 1);
        final int randomColumn = Tools.random(0, columns - 1);

        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                final double x = 150 * (i + 0.6);
                final double y = 130 * (j + 0.6);

                this.values.add(new Block(x, y, i == randomRow && j == randomColumn));
            }
        }
    }

    public List<Block> values() {
        return values;
    }
}
