package ar.edu.ungs.game.blocks;

import ar.edu.ungs.game.environment.Environment;

import java.awt.*;

public final class BlockDrawer {
    private final Environment environment;

    public BlockDrawer(Environment environment) {
        this.environment = environment;
    }

    public void draw(Block block) {
        environment.drawRectangle(block.x(), block.y(), Block.WIDTH(), Block.HEIGHT(), 0, Color.GREEN);
    }
}
