package ar.edu.ungs.game.blocks;

import ar.edu.ungs.game.environment.Environment;

import java.io.IOException;

public final class BlocksDrawer {
    private final BlockDrawer drawer;

    public BlocksDrawer(Environment environment) {
        drawer = new BlockDrawer(environment);
    }

    public void draw(Blocks blocks) throws IOException {
        for (Block item : blocks.values())
            drawer.draw(item);
    }
}
