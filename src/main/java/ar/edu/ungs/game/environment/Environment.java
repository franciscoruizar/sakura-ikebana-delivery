package ar.edu.ungs.game.environment;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;

public final class Environment extends JFrame {
    private final Board board;

    public Environment(GameInterface gameInterface, String title, int height, int width) {
        this.board = new Board(gameInterface);
        setup(title, height, width);
    }

    public void setup(String title, int height, int width) {
        this.board.setSize(width, height);
        this.add(this.board, "Center");
        this.pack();
        Insets ins = this.getInsets();
        this.setSize(width + ins.left + ins.right, width + ins.bottom + ins.top);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setTitle(title);
        this.setVisible(true);
    }

    public void drawImage(Image image, double x, double y, double angle) {
        this.drawImage(image, x, y, angle, 1.0D);
    }

    public void drawImage(Image image, double x, double y, double angle, double scala) {
        this.drawImageWithCenter(image, x, y, (double) (image.getWidth(null) / 2), (double) (image.getHeight(null) / 2), angle, scala);
    }

    public void drawImageWithCenter(Image image,
                                    double x,
                                    double y,
                                    double xCenter,
                                    double yCenter,
                                    double angle,
                                    double scala) {
        Graphics2D g2d = board.g2d();
        if (g2d != null) {
            AffineTransform transform = AffineTransform.getTranslateInstance(x, y);
            transform.concatenate(AffineTransform.getRotateInstance(angle));
            transform.concatenate(AffineTransform.getTranslateInstance(-scala * xCenter, -scala * yCenter));
            if (scala != 1.0D) {
                transform.concatenate(AffineTransform.getScaleInstance(scala, scala));
            }

            g2d.drawImage(image, transform, null);
        }
    }

    public void drawCircle(double x, double y, double diameter, Color color) {
        Graphics2D g2d = board.g2d();
        if (g2d != null) {
            Ellipse2D.Double circle = new Ellipse2D.Double(x - diameter / 2.0D, y - diameter / 2.0D, diameter, diameter);
            g2d.setPaint(color);
            g2d.fill(circle);
        }
    }

    public void drawRectangle(double x, double y, double width, double height, double angle, Color color) {
        Shape           rect = new Rectangle((int) x, (int) y, (int) width, (int) height);
        AffineTransform at   = AffineTransform.getTranslateInstance(x, y);
        at.concatenate(AffineTransform.getRotateInstance(angle));
        at.concatenate(AffineTransform.getTranslateInstance(-x - width / 2.0D, -y - height / 2.0D));
        Shape      rotateShape = at.createTransformedShape(rect);
        Graphics2D g2d         = board.g2d();
        if (g2d != null) {
            g2d.setPaint(color);
            g2d.fill(rotateShape);
        }
    }

    public void drawTriangle(double x, double y, int height, int base, double angle, Color color) {
        Polygon t = new Polygon();
        t.addPoint((int) x - height / 2, (int) y - base / 2);
        t.addPoint((int) x + height / 2, (int) y);
        t.addPoint((int) x - height / 2, (int) y + base / 2);
        AffineTransform at = AffineTransform.getTranslateInstance(x, y);
        at.rotate(angle);
        at.concatenate(AffineTransform.getTranslateInstance(-x, -y));
        Shape      rotateTriangle = at.createTransformedShape(t);
        Graphics2D g2d            = board.g2d();
        if (g2d != null) {
            g2d.setPaint(color);
            g2d.fill(rotateTriangle);
        }
    }

    public void writeText(String text, double x, double y) {
        Graphics2D g2d = board.g2d();
        if (g2d != null) {
            g2d.drawString(text, (int) x, (int) y);
        }
    }

    public void changeFont(String font, int size, Color color) {
        Graphics2D g2d = board.g2d();
        if (g2d != null) {
            g2d.setColor(color);
            g2d.setFont(new Font(font, Font.PLAIN, size));
        }
    }

    public boolean isPressed(Key key) {
        return this.board.isPressed(Key.value(key));
    }

    public boolean wasPressed(Key key) {
        return this.board.wasPressed(Key.value(key));
    }

    public void start() {
        this.board.start();
    }

    public int width() {
        return this.board.getWidth();
    }

    public int height() {
        return board.getHeight();
    }


    public Timer timer() {
        return board.timer();
    }
}
