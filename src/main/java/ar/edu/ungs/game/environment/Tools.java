package ar.edu.ungs.game.environment;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;
import java.awt.*;
import java.util.Random;

public final class Tools {
    public static double toRadians(double degrees) {
        return degrees / 360.0D * 6.283185307179586D;
    }

    public static double toDegrees(double radians) {
        return radians * 360.0D / 6.283185307179586D;
    }

    public static Image loadImage(String file) {
        return (new ImageIcon(ClassLoader.getSystemResource(file))).getImage();
    }

    public static void playClip(String file) {
        loadClip(file).start();
    }

    public static void loopClip(String file) {
        loadClip(file).loop(-1);
    }

    public static Clip loadClip(String file) {
        try {
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(ClassLoader.getSystemResource(file));
            Clip             clip    = AudioSystem.getClip();
            clip.open(audioIn);
            return clip;
        } catch (Exception exception) {
            throw new RuntimeException("load file error");
        }
    }

    public static int random(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    public static boolean random() {
        return random(0, 1) == 1;
    }
}
