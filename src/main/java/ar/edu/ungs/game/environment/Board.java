package ar.edu.ungs.game.environment;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Arrays;

public final class Board extends JPanel implements ActionListener {
    private final GameInterface gameInterface;
    private final boolean[]     keys;
    private final boolean[]     keysAtTheMoment;
    private       Timer         timer;
    private       Graphics2D    g2d;
    private       boolean       isStarted = false;

    public Board(GameInterface gameInterface) {
        this.gameInterface   = gameInterface;
        this.keys            = new boolean[256];
        this.keysAtTheMoment = new boolean[256];

        setup();
    }

    public void setup() {
        this.addKeyListener(new Board.BoardKeyAdapter());
        this.setFocusable(true);
        this.setBackground(Color.BLACK);
        this.setDoubleBuffered(true);
    }

    public void start() {
        this.isStarted = true;
        this.timer     = new Timer(10, this);
        this.timer.start();
    }

    public void paint(Graphics graphics) {
        super.paint(graphics);
        this.g2d = (Graphics2D) graphics;

        if (this.isStarted) {
            this.gameInterface.tick();
        }

        Arrays.fill(this.keysAtTheMoment, false);

        Toolkit.getDefaultToolkit().sync();
        graphics.dispose();
        this.g2d = null;
    }

    public boolean isPressed(char key) {
        return pressed(key, keys);
    }

    public boolean wasPressed(char key) {
        return pressed(key, keysAtTheMoment);
    }

    private boolean pressed(char key, boolean[] chars) {
        if ('a' <= key && key <= 'z') {
            key = (char) (key - 32);
        }

        if (key < chars.length) {
            return chars[key];
        } else {
            throw new RuntimeException("Error! Se consultó si la tecla " + key + " está presionada, pero esa tecla no existe.");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.repaint();
    }

    public Graphics2D g2d() {
        return g2d;
    }

    public Timer timer() {
        return timer;
    }

    private class BoardKeyAdapter extends KeyAdapter {
        public void keyReleased(KeyEvent e) {
            int key = e.getKeyCode();
            if (key >= 0 && key < Board.this.keys.length) {
                Board.this.keys[key]            = false;
                Board.this.keysAtTheMoment[key] = false;
            }

        }

        public void keyPressed(KeyEvent e) {
            int key = e.getKeyCode();
            if (key >= 0 && key < Board.this.keys.length) {
                Board.this.keysAtTheMoment[key] = !Board.this.keys[key];

                Board.this.keys[key] = true;
            }

        }
    }
}
