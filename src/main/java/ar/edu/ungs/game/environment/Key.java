package ar.edu.ungs.game.environment;

public enum Key {
    UP('&'),
    DOWN('('),
    RIGHT('\''),
    LEFT('%'),
    ENTER('\n'),
    CTRL((char) 17),
    ALT((char) 18),
    SHIFT((char) 16),
    SPACE(' '),
    INSERT((char) 155),
    DELETE((char) 127),
    HOME('$'),
    END('#'),
    EMPTY('#');

    private final char value;

    Key(char value) {
        this.value = value;
    }

    public static char value(Key key) {
        for (Key item : Key.values()) {
            if (item.equals(key)) {
                return key.value;
            }
        }
        return Key.EMPTY.value;
    }
}
