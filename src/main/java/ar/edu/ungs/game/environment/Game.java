package ar.edu.ungs.game.environment;

import ar.edu.ungs.game.blocks.Blocks;
import ar.edu.ungs.game.blocks.BlocksDrawer;
import ar.edu.ungs.game.coins.Coin;
import ar.edu.ungs.game.coins.CoinDrawer;
import ar.edu.ungs.game.collisions.*;
import ar.edu.ungs.game.enemies.Enemies;
import ar.edu.ungs.game.enemies.EnemiesDrawer;
import ar.edu.ungs.game.enemies.EnemiesMotionTrigger;
import ar.edu.ungs.game.flower_shop.FlowerShop;
import ar.edu.ungs.game.flower_shop.FlowerShopDrawer;
import ar.edu.ungs.game.houses.HousesDrawer;
import ar.edu.ungs.game.player.Player;
import ar.edu.ungs.game.player.PlayerDrawer;
import ar.edu.ungs.game.player.PlayerMotionTrigger;
import ar.edu.ungs.game.scores.*;
import ar.edu.ungs.game.street.StreetDrawer;

import java.awt.*;
import java.io.IOException;

public final class Game implements GameInterface {
    private static final String TITLE   = "Sakura Ikebana Delivery - Grupo 01 - v1";
    private static final int    HEIGHT  = 800;
    private static final int    WIDTH   = 800;
    private static final int    ROWS    = 6;
    private static final int    COLUMNS = 5;
    private static final int    SPACE_X = 150;
    private static final int    SPACE_Y = 130;

    private final Environment environment;

    private final Player                     player;
    private final Blocks                     blocks;
    private final Enemies                    enemies;
    private final Coin                       coin;
    private final FlowerShop                 shop;
    private final ScoreBoardEnemiesKilled    scoreBoardEnemiesKilled;
    private final ScoreBoardFlowersDelivered scoreBoardFlowersDelivered;
    private final ScoreBoardLives            scoreBoardLives;

    private final PlayerDrawer                     playerDrawer;
    private final BlocksDrawer                     blocksDrawer;
    private final StreetDrawer                     streetDrawer;
    private final EnemiesDrawer                    enemiesDrawer;
    private final HousesDrawer                     housesDrawer;
    private final CoinDrawer                       coinDrawer;
    private final FlowerShopDrawer                 shopDrawer;
    private final ScoreBoardEnemiesKilledDrawer    scoreBoardEnemiesKilledDrawer;
    private final ScoreBoardFlowersDeliveredDrawer scoreBoardFlowersDeliveredDrawer;
    private final ScoreBoardLivesDrawer            scoreBoardLivesDrawer;

    private final PlayerMotionTrigger  playerMotionTrigger;
    private final EnemiesMotionTrigger enemiesMotionTrigger;

    private final PlayerBlocksColliderCalculator     playerBlocksColliderCalculator;
    private final PlayerEnemiesColliderCalculator    playerEnemiesColliderCalculator;
    private final PowerEnemiesColliderCalculator     powerEnemiesColliderCalculator;
    private final PowerBlocksColliderCalculator      powerBlocksColliderCalculator;
    private final PlayerCoinColliderCalculator       playerCoinColliderCalculator;
    private final PlayerFlowerShopColliderCalculator playerFlowerShopColliderCalculator;

    private Game() {
        this.environment = new Environment(this, TITLE, HEIGHT, WIDTH);

        this.player                     = new Player(initialPosition().getX(), initialPosition().getY());
        this.blocks                     = new Blocks(ROWS, COLUMNS);
        this.enemies                    = new Enemies(ROWS, SPACE_X, SPACE_Y);
        this.coin                       = new Coin(WIDTH / 2);
        this.shop                       = new FlowerShop(environment.width() - 25, environment.height() - 30);
        this.scoreBoardEnemiesKilled    = new ScoreBoardEnemiesKilled(0, 0);
        this.scoreBoardFlowersDelivered = new ScoreBoardFlowersDelivered(WIDTH, 0);
        this.scoreBoardLives            = new ScoreBoardLives(WIDTH, 70, player);


        this.playerDrawer                     = new PlayerDrawer(environment);
        this.blocksDrawer                     = new BlocksDrawer(environment);
        this.streetDrawer                     = new StreetDrawer(environment);
        this.enemiesDrawer                    = new EnemiesDrawer(environment);
        this.housesDrawer                     = new HousesDrawer(environment);
        this.coinDrawer                       = new CoinDrawer(environment);
        this.shopDrawer                       = new FlowerShopDrawer(environment);
        this.scoreBoardEnemiesKilledDrawer    = new ScoreBoardEnemiesKilledDrawer(environment);
        this.scoreBoardFlowersDeliveredDrawer = new ScoreBoardFlowersDeliveredDrawer(environment);
        this.scoreBoardLivesDrawer            = new ScoreBoardLivesDrawer(environment);


        this.playerMotionTrigger  = new PlayerMotionTrigger(environment);
        this.enemiesMotionTrigger = new EnemiesMotionTrigger(environment);

        this.playerBlocksColliderCalculator     = new PlayerBlocksColliderCalculator();
        this.playerEnemiesColliderCalculator    = new PlayerEnemiesColliderCalculator(environment);
        this.powerEnemiesColliderCalculator     = new PowerEnemiesColliderCalculator();
        this.powerBlocksColliderCalculator      = new PowerBlocksColliderCalculator();
        this.playerCoinColliderCalculator       = new PlayerCoinColliderCalculator();
        this.playerFlowerShopColliderCalculator = new PlayerFlowerShopColliderCalculator();

        this.environment.start();
    }

    public static void start() {
        new Game();
    }

    @Override
    public void tick() {
        try {
            streetDrawer.draw();

            blocksDrawer.draw(blocks);
            housesDrawer.draw(blocks);

            coinDrawer.draw(coin);

            shopDrawer.draw(shop);

            enemies.ensureLives();
            enemiesDrawer.draw(enemies);
            enemiesMotionTrigger.trig(enemies);

            playerDrawer.draw(player);
            playerMotionTrigger.trig(player, false);

            playerBlocksColliderCalculator.calculate(player, blocks, scoreBoardFlowersDelivered, playerMotionTrigger);
            playerEnemiesColliderCalculator.calculate(player, enemies);
            playerFlowerShopColliderCalculator.calculate(player, shop, playerMotionTrigger);
            powerEnemiesColliderCalculator.calculate(enemies, player, scoreBoardEnemiesKilled);
            powerBlocksColliderCalculator.calculate(blocks, player);
            playerCoinColliderCalculator.calculate(player, coin, scoreBoardFlowersDelivered, ROWS, SPACE_X, SPACE_Y);

            scoreBoardEnemiesKilledDrawer.draw(scoreBoardEnemiesKilled);
            scoreBoardFlowersDeliveredDrawer.draw(scoreBoardFlowersDelivered);
            scoreBoardLivesDrawer.draw(scoreBoardLives);

            environment.changeFont(Font.SERIF, 20, Color.BLACK);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Point initialPosition() {
        return new Point(WIDTH / 2, HEIGHT / 2);
    }
}
