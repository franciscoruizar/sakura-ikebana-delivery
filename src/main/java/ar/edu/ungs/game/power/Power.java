package ar.edu.ungs.game.power;

public final class Power {
    private static final int    STEPS  = 5;
    private static final int    HEIGHT = 20;
    private static final int    WIDTH  = 22;
    private              double x;
    private              double y;

    public Power(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static int STEPS() {
        return STEPS;
    }

    public static int HEIGHT() {
        return HEIGHT;
    }

    public static int WIDTH() {
        return WIDTH;
    }

    public double x() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void accumulateX(double value) {
        this.x += value;
    }

    public void deAccumulateX(double value) {
        this.x -= value;
    }

    public double y() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void accumulateY(double value) {
        this.y += value;
    }

    public void deAccumulateY(double value) {
        this.y -= value;
    }
}
