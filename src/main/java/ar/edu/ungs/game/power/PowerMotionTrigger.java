package ar.edu.ungs.game.power;

import ar.edu.ungs.game.environment.Environment;
import ar.edu.ungs.game.environment.Key;
import ar.edu.ungs.game.player.Player;

public final class PowerMotionTrigger {
    private final Environment environment;
    private       Key         lastKey;

    public PowerMotionTrigger(Environment environment) {
        this.environment = environment;
        this.lastKey     = Key.EMPTY;
    }

    public void trig(Player player) {
        if (!player.hasPower()) {
            return;
        }

        if (isOutRange(player.power())) {
            player.deletePower();
            return;
        }

        executeAction(player);
    }

    private void executeAction(Player player) {
        switch (lastKey) {
            case UP:
                up(player.power());
                break;
            case DOWN:
                down(player.power());
                break;
            case RIGHT:
                right(player.power());
                break;
            case LEFT:
                left(player.power());
                break;
        }
    }

    private void right(Power power) {
        if (power.x() < environment.width()) {
            power.accumulateX(Power.STEPS());
        }
    }

    private void left(Power power) {
        if (power.x() > 0) {
            power.deAccumulateX(Power.STEPS());
        }
    }

    private void up(Power power) {
        if (power.y() > 0) {
            power.deAccumulateY(Power.STEPS());
        }
    }

    private void down(Power power) {
        if (power.y() < environment.height()) {
            power.accumulateY(Power.STEPS());
        }
    }

    private boolean isOutRange(Power power) {
        return power.x() <= 0 || power.x() >= environment.width() || power.y() <= 0 || power.y() >= environment.height() - 10;
    }

    public void setLastKey(Key lastKey) {
        this.lastKey = lastKey;
    }
}
