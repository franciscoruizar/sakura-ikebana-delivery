package ar.edu.ungs.game.power;

import ar.edu.ungs.game.environment.Environment;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public final class PowerDrawer {
    private static final String POWER_FILE = "src/main/resources/power.png";

    private final Environment environment;

    public PowerDrawer(Environment environment) {
        this.environment = environment;
    }

    public void draw(Power power) throws IOException {
        drawPowerImage(environment, power);
    }

    private void drawPowerImage(Environment environment, Power power) throws IOException {
        Image image = ImageIO.read(new File(POWER_FILE));
        environment.drawImage(image, power.x(), power.y(), 0);
    }
}
