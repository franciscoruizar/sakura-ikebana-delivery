package ar.edu.ungs.game.enemies;

import ar.edu.ungs.game.environment.Environment;

public final class EnemyMotionTrigger {
    private final Environment environment;

    public EnemyMotionTrigger(Environment environment) {
        this.environment = environment;
    }

    public void trig(Enemy enemy) {
        final double limitWidth  = environment.width();
        final double limitHeight = environment.height() + 15;

        if (enemy.isInitialX()) {
            if (enemy.y() <= limitWidth) {
                enemy.accumulateY(enemy.STEPS());
            } else {
                enemy.setY(0);
            }
        } else {
            if (enemy.x() <= limitHeight) {
                enemy.accumulateX(enemy.STEPS());
            } else {
                enemy.setX(0);
            }
        }
    }
}
