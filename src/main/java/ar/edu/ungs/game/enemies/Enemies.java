package ar.edu.ungs.game.enemies;

import java.time.LocalTime;

public final class Enemies {
    private final static Integer DISABLE_TIME = 10;
    private final        Integer valueSize, spaceBetweenX, spaceBetweenY;
    private final Enemy[]     values;
    private final LocalTime[] valuesKill;

    public Enemies(Integer valueSize, Integer spaceBetweenX, Integer spaceBetweenY) {
        this.valueSize     = valueSize;
        this.spaceBetweenX = spaceBetweenX;
        this.spaceBetweenY = spaceBetweenY;

        this.values     = new Enemy[valueSize * 2];
        this.valuesKill = new LocalTime[valueSize * 2];


        for (int i = 0; i < values.length; i++) {
            valuesKill[i] = null;

            create(i);
        }
    }

    public Enemy[] values() {
        return values;
    }

    public void kill(int index) {
        values[index]     = null;
        valuesKill[index] = LocalTime.now().plusSeconds(DISABLE_TIME);
    }

    private void create(int i) {
        if (i < valueSize) {
            values[i] = new Enemy(i * spaceBetweenX + 10, 0);
        } else {
            values[i] = new Enemy(0, (i - valueSize) * spaceBetweenY + 10);
        }
    }

    public void ensureLives() {
        LocalTime time = LocalTime.now();

        for (int i = 0; i < valuesKill.length; i++) {
            if (valuesKill[i] != null && time.isAfter(valuesKill[i])) {
                valuesKill[i] = null;
                create(i);
            }
        }
    }
}
