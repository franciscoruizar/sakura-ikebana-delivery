package ar.edu.ungs.game.enemies;

import java.util.Random;

public final class Enemy {
    private final static double STEPS  = 1;
    private static final int    HEIGHT = 20;
    private static final int    WIDTH  = 22;

    private final boolean initialX;
    private final boolean isHard;
    private       double  y;
    private       double  x;

    public Enemy(double x, double y) {
        this.x        = x;
        this.y        = y;
        this.initialX = x > 0;
        this.isHard   = new Random().nextBoolean();
    }

    public static int HEIGHT() {
        return HEIGHT;
    }

    public static int WIDTH() {
        return WIDTH;
    }

    public double STEPS() {
        return isHard ? STEPS * 2 : STEPS;
    }

    public double x() {
        return x;
    }

    public double y() {
        return y;
    }

    public boolean isInitialX() {
        return initialX;
    }

    public void accumulateX(double value) {
        this.x += value;
    }

    public void accumulateY(double value) {
        this.y += value;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }
}
