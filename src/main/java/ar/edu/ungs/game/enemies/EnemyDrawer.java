package ar.edu.ungs.game.enemies;

import ar.edu.ungs.game.environment.Environment;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public final class EnemyDrawer {
    private final static String      ENEMY_FILE = "src/main/resources/ninja.png";
    private final        Environment environment;

    public EnemyDrawer(Environment environment) {
        this.environment = environment;
    }

    public void draw(Enemy enemy) throws IOException {
        Image image = ImageIO.read(new File(ENEMY_FILE));
        environment.drawImage(image, enemy.x(), enemy.y(), 0);
    }
}
