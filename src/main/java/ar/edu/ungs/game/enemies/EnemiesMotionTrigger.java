package ar.edu.ungs.game.enemies;

import ar.edu.ungs.game.environment.Environment;

public final class EnemiesMotionTrigger {
    private final EnemyMotionTrigger motionTrigger;

    public EnemiesMotionTrigger(Environment environment) {
        this.motionTrigger = new EnemyMotionTrigger(environment);
    }

    public void trig(Enemies enemies) {
        for (Enemy item : enemies.values()) {
            if (item != null) {
                motionTrigger.trig(item);
            }
        }
    }
}
