package ar.edu.ungs.game.enemies;

import ar.edu.ungs.game.environment.Environment;

import java.io.IOException;

public final class EnemiesDrawer {
    private final EnemyDrawer drawer;

    public EnemiesDrawer(Environment environment) {
        this.drawer = new EnemyDrawer(environment);
    }

    public void draw(Enemies enemies) throws IOException {
        for (Enemy item : enemies.values()) {
            if (item != null) {
                drawer.draw(item);
            }
        }
    }
}
