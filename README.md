# Trabajo Practico: Sakura Ikebana Delivery

- **Materia**: Programación I
- **Alumnos**: Marco Zucchi & Francisco Ruiz Lezcano
- **Comisión**: 4
- **Turno**: Noche

# Introduccion

Sakura Haruno junto a su amiga de la infancia Ino Yamanaka, tienen un negocio de arreglos florales típicos japoneses (
ikebana). Ino se encarga de preparar los arreglos florales, y Sakura se encarga del delivery de los mismos.

La florerería Yamanaka-Haruno está ubicada en una aldea ninja, que suele ser atacada por ninjas enemigos. Esto complica
el trabajo de Sakura que debe entregar los pedidos evitando y combatiendo a los ninjas enemigos.

El objetivo de este video juego es que Sakura entregue los pedidos sin perder la vida a manos de los ninjas enemigos.

![docs/Untitled.png](docs/Untitled.png)

## Requisitos

- Al iniciar el juego, el jugado debe aparecer en el centro de la pantalla ✔️
- El jugador solo puede moverse en una sola direccion y sobre las calles ✔️
- Admitir colisiones entre el jugador y cuadras de casas, siendo que Sakura no puede pasar por encima de estas ultimas,
  ✔️
- Elegir una cuadra de casas aleatoriamente por cada vez que el jugador entrega la flor , siendo distinguida por el
  resto. ✔️
- Enemigos que se mueven en una sola direccion ✔️
- Si un enemigo colisiona con el jugador, juego terminado ✔️
- El jugador debe tener la capacidad de lanzar un poder para eliminar enemigo ✔️
- Diferentes dificultades de enemigos ✔️
- Florerería: Sakura debe buscar el ikebana en la florería antes de llevarlo a la siguientecasa.✔️
- Tableros de puntaje:
    - Contador de enemigos eliminados ✔️
    - Contador de flores entregadas ✔️

# Como empezar

1. Tener instalado Java 8
2. Clonar
   repositorio `git clone [https://gitlab.com/franciscoruizar/sakura-ikebana-delivery](https://gitlab.com/franciscoruizar/sakura-ikebana-delivery)`

   [Francisco Ruiz / Sakura Ikebana Delivery](https://gitlab.com/franciscoruizar/sakura-ikebana-delivery)

3. Compilar el proyecto:
    1. **Eclipse**:
        1. Click en File → Import ▸ Existing Gradle Project.
        2. Buscar la carpeta del proyecto e importar
        3. Buscar el fichero `Starter.class` → Click derecho → Run As → Java Application
    2. Ejecutando jar: Desde la carpeta del proyecto: `java -jar build/libs/sakura-ikebana-delivery-1.0-SNAPSHOT.jar`

## Estructura del proyecto

```jsx
.
├── build
├── build.gradle
├── docs
├── gradle
├── gradlew
├── gradlew.bat
├── Makefile
├── README.md
├── settings.gradle
└── src
    └── main
        ├── java
        │   └── ar
        │       └── edu
        │           └── ungs
        │               └── game
        │                   ├── blocks
        │                   │   ├── BlockDrawer.java
        │                   │   ├── Block.java
        │                   │   ├── BlocksDrawer.java
        │                   │   └── Blocks.java
        │                   ├── coins
        │                   │   ├── CoinDrawer.java
        │                   │   ├── Coin.java
        │                   │   └── CoinPositionCalculator.java
        │                   ├── collisions
        │                   │   ├── PlayerBlockColliderCalculator.java
        │                   │   ├── PlayerBlocksColliderCalculator.java
        │                   │   ├── PlayerCoinColliderCalculator.java
        │                   │   ├── PlayerEnemiesColliderCalculator.java
        │                   │   ├── PlayerEnemyColliderCalculator.java
        │                   │   ├── PlayerFlowerShopColliderCalculator.java
        │                   │   ├── PowerEnemiesColliderCalculator.java
        │                   │   └── PowerEnemyColliderCalculator.java
        │                   ├── enemies
        │                   │   ├── EnemiesDrawer.java
        │                   │   ├── Enemies.java
        │                   │   ├── EnemiesMotionTrigger.java
        │                   │   ├── EnemyDrawer.java
        │                   │   ├── Enemy.java
        │                   │   └── EnemyMotionTrigger.java
        │                   ├── environment
        │                   │   ├── Board.java
        │                   │   ├── Environment.java
        │                   │   ├── GameInterface.java
        │                   │   ├── Game.java
        │                   │   ├── Key.java
        │                   │   └── Tools.java
        │                   ├── flower_shop
        │                   │   ├── FlowerShopDrawer.java
        │                   │   └── FlowerShop.java
        │                   ├── player
        │                   │   ├── PlayerDrawer.java
        │                   │   ├── Player.java
        │                   │   └── PlayerMotionTrigger.java
        │                   ├── power
        │                   │   ├── PowerDrawer.java
        │                   │   ├── Power.java
        │                   │   └── PowerMotionTrigger.java
        │                   ├── scores
        │                   │   ├── ScoreBoardEnemiesKilledDrawer.java
        │                   │   ├── ScoreBoardEnemiesKilled.java
        │                   │   ├── ScoreBoardFlowersDeliveredDrawer.java
        │                   │   └── ScoreBoardFlowersDelivered.java
        │                   ├── Starter.java
        │                   └── street
        │                       └── StreetDrawer.java
        └── resources
            ├── flower.png
            ├── flower-shop.png
            ├── ninja.png
            ├── player.png
            └── power.png
```

### Nomenclaturas

Clases con el sufijo **Drawer:** Unica responsabilidad de dibujar al domino dentro del entorno del juego

Clases con el sufijo **ColliderCalculator:** Responsabilidad de calcular las colisiones entre los diferentes dominios, y
dependiendo del resultado cambiar el estado de movimientos del juego

Clases con el sufijo **MotionTrigger:** Responsabilidad de mover al dominio en base a su logica de negocios y estado.

### Modulos y clases implementadas

- `blocks`

    ```bash
    .
    ├── BlockDrawer.java
    ├── Block.java
    ├── BlocksDrawer.java
    └── Blocks.java
    ```

- `coins`:

    ```bash
    .
    ├── CoinDrawer.java
    ├── Coin.java
    └── CoinPositionCalculator.java
    ```

- `collisions`:

    ```bash
    .
    ├── PlayerBlockColliderCalculator.java
    ├── PlayerBlocksColliderCalculator.java
    ├── PlayerCoinColliderCalculator.java
    ├── PlayerEnemiesColliderCalculator.java
    ├── PlayerEnemyColliderCalculator.java
    ├── PlayerFlowerShopColliderCalculator.java
    ├── PowerEnemiesColliderCalculator.java
    └── PowerEnemyColliderCalculator.java
    ```

- `enemies`:

    ```bash
    .
    ├── EnemiesDrawer.java
    ├── Enemies.java
    ├── EnemiesMotionTrigger.java
    ├── EnemyDrawer.java
    ├── Enemy.java
    └── EnemyMotionTrigger.java
    ```

- `environment`

    ```bash
    .
    ├── Board.java
    ├── Environment.java
    ├── GameInterface.java
    ├── Game.java
    ├── Key.java
    └── Tools.java
    ```

- `flower_shop`

    ```bash
    .
    ├── FlowerShopDrawer.java
    └── FlowerShop.java
    ```

- `player`:

    ```bash
    .
    ├── PlayerDrawer.java
    ├── Player.java
    └── PlayerMotionTrigger.java
    ```

- `power`:

    ```bash
    .
    ├── PowerDrawer.java
    ├── Power.java
    └── PowerMotionTrigger.java
    ```

- `scores`:

    ```bash
    .
    ├── ScoreBoardEnemiesKilledDrawer.java
    ├── ScoreBoardEnemiesKilled.java
    ├── ScoreBoardFlowersDeliveredDrawer.java
    └── ScoreBoardFlowersDelivered.java
    ```

- `street`

    ```bash
    .
    └── StreetDrawer.java
    ```

## Principios de diseños aplicados: Responsabilidad única

El principio de responsabilidad única establece que cada módulo o clase debe tener responsabilidad sobre una sola parte
de la funcionalidad proporcionada por el software y esta responsabilidad debe estar encapsulada en su totalidad por la
clase.

A la hora de desarrollar el proyecto, se ha determinado que cada entidad tendria clases con distintas y únicas
responsabilidades de funcionamiento, para que el ciclo de vida y funcionamiento sea legible, escalable y funcional.
Siendo que cada módulo tendria clases que determinan el modelo de la entidad, los movimiento y dibujo en el entorno del
juego. Tales: `player`, `enemies`, `blocks` y  `power`. Por otra parte tenemos un modulo de `collisions` que solo
calcula los choques entre las diferentes entidades relacionadas.

# Conclusiones

Fue un trabajo muy fructuoso para la organizacion en equipo, división de tareas y puesta de objetivos. Desde las
decisiones de equipo de trabajar con nomenclaturas en inglés, aportar valor al dominio con la estructura del proyecto
hasta utilización GIT fue una evolución constante de aprendizaje.

